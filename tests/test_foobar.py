from foobar.bar import hello_bar
from foobar.bar import hello_bar
from foobar.foo import hello_foo
from foobar.foo import hello_fooblah
from foobar.subfoo.blah import hello_blah

def test_bar():
    hello_bar()

def test_foo():
    hello_foo()

def test_blah():
    hello_blah()

def test_fooblah():
    hello_fooblah()
