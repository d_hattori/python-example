# Python Example

## build
```
pip install .
```

## test
```
cd test
pytest .
```

## Proper naming conventions

| 対象 | ルール | 例 |
| ---- | ---- | ----- |
| パッケージ | 全小文字 なるべく短くアンダースコア非推奨 | tqdm, requests ... |
| モジュール | 全小文字 なるべく短くアンダースコア可 | sys, os,... |
| クラス | 最初大文字 + 大文字区切り | MyFavoriteClass |
| 例外 | 最初大文字 + 大文字区切り |MyFuckingError |
| 型変数 | 最初大文字 + 大文字区切り |	MyFavoriteType |
| メソッド | 全小文字 + アンダースコア区切り | my_favorite_method |
| 関数 | 全小文字 + アンダースコア区切り |	my_favorite_funcion |
| 変数 | 全小文字 + アンダースコア区切り |	my_favorite_instance |
| 定数 | 全大文字 + アンダースコア区切り |	MY_FAVORITE_CONST |

## Reference
https://pep8-ja.readthedocs.io/ja/latest/
https://docs.python.org/ja/3/distutils/examples.html
